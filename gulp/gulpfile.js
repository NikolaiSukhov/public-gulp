const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass')(require('sass'));

const paths = {
    styles: {
      src: 'dist/scss/**/*.scss',
      dest: 'dist/css/'
    },
    scripts: {
      src: 'dist/scripts/**/*.js',
      dest: 'dist/js/'
    }
  };
gulp.task('styles', () => {
  return gulp.src('src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('serve', function() {
  browserSync.init({
    server: "./dist"
  });

  gulp.watch("src/scss/*.scss", gulp.series('sass'));
  gulp.watch("dist/*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
  return gulp.src("src/scss/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.stream());
});

gulp.task('stream', function() {
  return gulp.watch('src/**/*.scss', gulp.series('styles'));
});
function concatJS() {
    return gulp.src(paths.scripts.src)
      .pipe(concat('scripts.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(paths.scripts.dest));
  }
  exports.concat = concatJS;
  function minifyCSS() {
    return gulp.src(paths.styles.dest + '*.css')
      .pipe(cleanCSS())
      .pipe(gulp.dest(paths.styles.dest));
  }
  function prefixCSS() {
  }
  
  function styles() {
    return gulp.src(paths.styles.src)
      .pipe(sass())
      .pipe(prefixCSS()) 
      .pipe(cleanCSS())
      .pipe(gulp.dest(paths.styles.dest))
      .pipe(browserSync.stream());
  }
  
  
gulp.task('dev', gulp.series( gulp.parallel('styles', 'sass'), gulp.parallel('stream', 'serve')));
exports.build = gulp.series( gulp.parallel(prefixCSS, concatJS), minifyCSS);
